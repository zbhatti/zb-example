// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

// Add a submodule for JetSelectionHelper
#include "JetSelectionHelper/JetSelectionHelper.h"

int main() {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.24604725._000008.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  // make histograms for storage
  TH1D *h_njets_raw = new TH1D("h_njets_raw","",20,0,20);
  TH1D *h_njets_kin = new TH1D("h_njets_kin","",20,0,20);
  
  TH1D *h_mjj_raw = new TH1D("h_mjj_raw","",100,0,500);
  TH1D *h_mjj_kin = new TH1D("h_mjj_kin","",100,0,500);

  
  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();
  
  // add jet selection helper
  JetSelectionHelper jet_selector;
  
  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    if (i % 1000 == 0){
      std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;
    }

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    // make temporary vector of jets for those which pass selection
    std::vector<xAOD::Jet> jets_raw;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event
      // std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;

      jets_raw.push_back(*jet);

    }
    
    // Kinematic cut jet container
    std::vector<xAOD::Jet> jets_kin;
    for(const xAOD::Jet* jet : *jets) {
      // perform kinematic selections and store in vector of "selected jets"
      if(jet_selector.isJetGood(jet)){
        jets_kin.push_back(*jet);
      }
    }
    
        
    // fill the analysis histograms accordingly
    h_njets_raw->Fill( jets_raw.size() );

    if( jets_raw.size()>=2 ){
      h_mjj_raw->Fill( (jets_raw.at(0).p4()+jets_raw.at(1).p4()).M()/1000. );
    }


    // fill the analysis histograms with the kin set of variables
    h_njets_kin->Fill( jets_kin.size() );

    if( jets_kin.size()>=2 ){
      h_mjj_kin->Fill( (jets_kin.at(0).p4()+jets_kin.at(1).p4()).M()/1000. );
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }  
  
  
  
  // canvas for drawing original histograms on 
  TCanvas *c1 = new TCanvas("c1","Example", 200,10,700,900); //last 4 arguments: top x-coord of window, top y-coord of window, x width, y width 
  TPad *pad1 = new TPad("pad1","The pad with the function", 0.05,0.50,0.95,0.95,21);  
  TPad *pad2 = new TPad("pad2","The pad with the histogram", 0.05,0.05,0.95,0.45,21);
  
  pad1->Draw();  
  pad2->Draw();
  
  pad1->cd();
  h_mjj_raw->Draw();
  
  pad2->cd();
  h_njets_raw->Draw();
  
  c1->Update(); 
  c1->SaveAs("plots.pdf");
  
  // canvas for drawing kinematic-based histograms on 
  TCanvas *c2 = new TCanvas("c2","Example", 200,10,700,900);
  TPad *pad3 = new TPad("pad3","The pad with the function", 0.05,0.50,0.95,0.95,21);  
  TPad *pad4 = new TPad("pad4","The pad with the histogram", 0.05,0.05,0.95,0.45,21);  
  
  // next set of plots
  pad3->Draw();
  pad4->Draw();
  
  pad3->cd();
  h_mjj_kin->Draw();
  
  pad4->cd();
  h_njets_kin->Draw();
  
  c2->Update();
  c2->SaveAs("plots2.pdf");
  
  
  // open TFile to store the analysis histogram output
  TFile *fout = new TFile("myOutputFile.root","RECREATE");

  h_njets_raw->Write();

  h_mjj_raw->Write();

  fout->Close();

  // exit from the main function cleanly
  return 0;
}

